//Zadanie 1
//Napisz metodę multiply wyznaczającą iloczyn dwóch zadanych liczb całkowitych.
//Działanie funkcji sprawdź pisząc odpowiedni program.

package zadania_metody;

public class Task1 {

    public static void main(String[] args) {
        int product = multiply(8, 2);
        System.out.println(product);
    }

    public static int multiply(int a, int b) {
        return a * b;
    }
}

