//Zadanie 13
//Stwórz program, który odczytuje dany napis i wypisuje ile razy w danym napisie
//występują małe litery. Przykładowo dla napisu: aAaaBssk wynikiem powinno być
//6 (małe a występuje 3 razy, s występuje 2 razy, k występuje 1 raz).

package zadania_pętle;

import java.util.Scanner;

public class Task13 {
    public static void main(String args[]) {
        System.out.println("Wiadomosc: ");
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        int duze = 0, male = 0, numery = 0, znaki = 0;

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch >= 'A' && ch <= 'Z')
                duze++;
            else if (ch >= 'a' && ch <= 'z')
                male++;
            else if (ch >= '0' && ch <= '9')
                numery++;
            else
                znaki++;
        }

        System.out.println("Malych liter : " + male);
        System.out.println("Duzych liter : " + duze);
        System.out.println("Cyfr : " + numery);
        System.out.println("Znakow specjalnych : " + znaki);
    }
}
