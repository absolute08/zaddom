//Zadanie2
//Napisz program, który odczytuje n i sumuje liczby od 1 do n.
package zadania_pętle;

import java.util.Scanner;

public class Task2 {
    public static void main(String args[]) {
        int i, sum = 0, z;
        Scanner sc = new Scanner(System.in);
        System.out.print("\n Ile liczb chcesz dodac: ");
        int n = sc.nextInt();
        for (i = 0; i < n; i++) {
            System.out.print("Liczba: ");
            z = sc.nextInt();
            sum = sum + z;
        }
        System.out.println("Suma liczb: " + sum);
    }
}