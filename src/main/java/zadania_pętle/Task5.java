//Zadanie 5
//Napisz program, który prosi o podanie poprawnego hasła( hasło to Polska), tak
//długo jak użytkownik nie odgadnie hasła wyświetlany jest komunikat podaj
//poprawne hasło.
package zadania_pętle;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        String haslo;
        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz hasło");
        haslo = sc.nextLine();
        while (!haslo.equals("Polska")) {
            System.out.println("Złe hasło. Wpisz poprawne hasło");
            haslo = sc.nextLine();
        }
        System.out.println("Poprawne hasło");
    }
}


