//Zadanie 10
//Napisz program drukujący na ekranie 19 gwiazdek:
//*******************
package zadania_pętle;

public class Task10 {
    public static void main(String[] args) {
        System.out.println("******************* :P");
        for (int i = 1; i <= 19; i++) {
            System.out.print("*");
        }
        System.out.println(" ");
    }
}