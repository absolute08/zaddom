//Zadanie 7 KONIECZNIE!!!
//Napisz program który wygeneruje za pomocą (wielkość wieżyczki podaje
//użytkownik)
//b) choinkę
//   *
//  ***
// *****
//*******
package zadania_pętle;

import java.util.Scanner;

public class Task7b {
    public static void main(String[] args) {
        char gwiazdka = '*';
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj ilosc poziomow choinki: ");
        int poziomy = sc.nextInt();
        for (int i = 0; i < poziomy; i++) {
            for (int j = 0; j < poziomy * 2; j++) {
                if (j < (poziomy - i) || j > (poziomy + i)) {
                    System.out.print(" ");
                } else {
                    System.out.print(gwiazdka);
                }
            }
            System.out.println();
        }
    }
}
