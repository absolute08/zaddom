//Zadanie 12
//Napisz program, który odczytuje wyraz i wypisuje go w odwrotnej kolejności.

package zadania_pętle;

import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        System.out.println("Wpisz wiadomosc do odwrocenia:");

        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String rev = "";


        for (int i = str.length() - 1; i >= 0; i--) {
            rev = rev + str.charAt(i);
        }

        System.out.println("Odwrocona wiadomosc to:");
        System.out.println(rev);
    }
}
