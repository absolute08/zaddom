//Zadanie 4
//Napisz program, który losuje 6 liczb z Dużego Lotka.
package zadania_pętle;

import java.util.Arrays;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class Task4 {
    public static void main(String[] args) {

        Set<Integer> numbers = new TreeSet<>();
        Random random = new Random();

        System.out.println("Wylosowane liczby to:\n");
        while (numbers.size() < 6) {
            int n = random.nextInt(50);
            if (n > 0) {
                numbers.add(n);
            }
        }
        System.out.println(Arrays.toString(numbers.toArray()));
    }
}