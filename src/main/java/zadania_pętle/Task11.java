//Zadanie 11
//Napisz program, który odczytuje liczbę i sprawdza czy liczba jest pierwsza czy
//złożona.
package zadania_pętle;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        int numer;
        boolean pierwsza = true;
        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz liczbe by sprawdzic czy jest pierwsza czy zlozona:");
        numer = Integer.parseInt(sc.nextLine());
        sc.close();

        if (numer < 1)
            System.out.println("Liczba musi byc wieksza od 1)");
        else if (numer == 1)
            System.out.println("1 nie jest ani liczba pierwsza ani zlozona");
        else {
            for (int dzielnik = 2; dzielnik <= (numer / 2); dzielnik++) {
                if ((numer % dzielnik) == 0) {
                    pierwsza = false;
                    break;

                }
            }
            if (pierwsza)
                System.out.printf("%d jest liczba pierwsza", numer);
            else System.out.printf("%d jest liczba zlozona", numer);
        }
    }
}