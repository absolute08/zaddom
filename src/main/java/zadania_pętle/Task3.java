//Zadanie 3
//Napisz prostą grę - zadaniem użytkownika będzie zgadnięcie liczby, którą
// zainicjujemy w programie (przykładowa liczba 600). W przypadku, gdy liczba
// będzie za duża lub za mała, użytkownik otrzyma odpowiednią podpowiedź.
// Gramy tak długo dopóki użytkownik zgadnie liczbę.
package zadania_pętle;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {

        int numer = (int) (1 + Math.random() * 100);

        System.out.println("You don't know me, but I know you. I want to play a game.");
        System.out.println("Here's what happens if you lose.");
        System.out.println("The device you are wearing is hooked into your upper and lower jaw.");
        System.out.println("When the timer in the back goes off, your mouth will be permanently ripped open.");
        System.out.println("Think of it like a reverse bear trap.");
        System.out.println("Here, I'll show you.");
        System.out.println("There is only one key to open the device.");
        System.out.println("You have to guess what number am I thinking of to get it");

        System.out.println("Please type a whole number and press enter.");

        Scanner scanner = new Scanner(System.in);

        int proby = 0;
        boolean done = false;
        while (!done) {

            int liczba = scanner.nextInt();
            proby++;

            if (numer < liczba) {
                System.out.println("The number I'm thinking of is lower than " + liczba + ".");
                System.out.println("Guess again! (Please type a whole number and press enter.)");
            } else if (numer > liczba) {
                System.out.println("The number I'm thinking of is greater than " + liczba + ".");
                System.out.println("Guess again! (Please type a whole number and press enter.)");
            } else {
                System.out.println("That's correct! I was thinking of a " + numer + ".");
                System.out.println("It took you " + proby + " guesses to get it right. Thanks for playing!");
                done = true;
            }
        }
    }
}

