//Zadanie 6
//Napisz program, który odczytuje n i oblicza n!.
package zadania_pętle;

import java.util.Scanner;

public class Task6 {
    static long kalkulatorsilnia(int n) {
        long silnia = 1;
        int i = 1;
        while (i <= n) {
            silnia = silnia * i;
            i++;
        }
        return silnia;
    }

    public static void main(String args[]) {
        int number;
        Scanner scan = new Scanner(System.in);
        System.out.println("Wpisz liczbe do obliczenia: ");
        number = scan.nextInt();
        System.out.println(kalkulatorsilnia(number));
    }
}
