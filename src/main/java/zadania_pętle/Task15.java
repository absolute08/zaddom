//Zadanie 15
//Napisz program, który oblicza największy wspólny dzielnik dwóch liczb.

package zadania_pętle;

import java.util.Scanner;

public class Task15 {
    private static int nwd(int a, int b) {
        int c = (a > b) ? a % b : b % a;
        if (c == 0) return Math.min(a, b);
        return (a > b) ? nwd(c, b) : nwd(c, a);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program obliczający największy wspólny dzielnik.");
        System.out.println("Podaj pierwszą liczbę: ");
        int a = scanner.nextInt();
        System.out.println("Podaj drugą liczbę: ");
        int b = scanner.nextInt();
        scanner.close();
        System.out.println("Największy wspólny dzielnik liczb " + a + " i " + b + " to: " + nwd(a, b));
    }

}