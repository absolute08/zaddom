//Zadanie 8
//Odczytaj wyraz i wypisz wszystkie cyfry występujące w wyrazie.
package zadania_pętle;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String string;

        System.out.println("Wpisz wiadomosc:");

        string = sc.nextLine();

        System.out.println("Cyfry znalezione w wiadomosci: ");

        for (char ch : string.toCharArray()) {
            if (Character.isDigit(ch)) {
                System.out.print(ch + " ");
            }
        }
    }

}
