//Zadanie 14
//Napisz program, który wypisuje co drugą literę imienia.

package zadania_pętle;

import java.util.Scanner;

public class Task14 {
    public static void main(String[] args) {
        System.out.println("Podaj imie:");
        Scanner sc = new Scanner(System.in);
        String slowo1 = sc.nextLine();

        String slowo2 = "";
        int length = slowo1.length();
        for (int i = 0; i <= length - 1; i += 2)
            slowo2 = slowo2 + slowo1.charAt(i);
        System.out.println(slowo2);
    }
}
