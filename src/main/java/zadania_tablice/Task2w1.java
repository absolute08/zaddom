//Zadanie 1
//Napisz metodę, która dla danej tablicy liczb całkowitych zwraca pierwszy element
//tablicy.
//first([1,2,3,4]) = 1
//first(4,9,12,1]) = 4


//Zadanie 2
//Napisz metodę, która dla danej tablicy liczb całkowitych zwraca ostatni element
//tablicy.

package zadania_tablice;

public class Task2w1 {
    public static void main(String[] args) {
        int[] tablica = {11, 32, 39, 42, 5};
        int first = tablica[0];
        int last = tablica[tablica.length - 1];
        System.out.println("pierwsza: " + first + "\n" + "ostatnia: " + last);
    }

}
