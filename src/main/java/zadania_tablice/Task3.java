//Zadanie 3
//Napisz metodę, która jako argument przyjmuje zawsze tablice składającą się z
//dwóch elementów liczb całkowitych. Metoda ma zwróci sumę elementów tablicy.
//sum([1,2]) = 3
//sum([4,8]) = 12

package zadania_tablice;

public class Task3 {
    public static void main(String[] args) {
        int[] tablica = {13, 41};
        int[] tablica2 = {32, 14};
        int sum2 = 0;
        int sum = 0;
        for (int i : tablica) {
            sum += i;
        }
        for (int i : tablica2) {
            sum2 += i;
        }

        System.out.println("Suma pierwszej tablicy: " + sum);
        System.out.println("Suma drugiej tablicy: " + sum2);
    }
}