//Zadanie 2.
//Odczytaj wyraz i wypisz na ekran wartość true lub false w
//zależności od tego czy wyraz zawiera w sobie napis pies.
package zadania_string;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        System.out.println("Wpisz słowo");
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String substring = "pies";
        System.out.println(str.contains(substring));
    }
}
