//Zadanie 1.
//Napisz program, który odczytuje wyraz i sprawdza czy pierwsza litera to J

//Dane:
//Jacek
//Wynik:
//true
//Dane2:
//Tomek
//Wynik:
//false
package zadania_string;

public class Task1 {
    public static void main(String[] args) {
        boolean name = Character.isUpperCase("tomek".charAt(0));
        boolean name2 = Character.isUpperCase("Tomek".charAt(0));
        System.out.println(name);
        System.out.println(name2);
    }
}


